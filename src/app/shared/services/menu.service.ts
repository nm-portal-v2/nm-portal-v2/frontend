import { Injectable, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
    private state$ = new BehaviorSubject<boolean>(false);
    state: boolean = false;
    sidenav: MatSidenav;

    constructor() {
    }

    setSidenav(sideNav) {
      this.sidenav = sideNav;
    }
}