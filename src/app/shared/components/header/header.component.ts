import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/Authentication/authServices/auth.service';
import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  constructor(private changeDetectorRef: ChangeDetectorRef, private menuService: MenuService, private authService: AuthService, private router: Router) {
  }
  isAuthenticated = false;
  private userSub: Subscription



  ngOnInit(): void {
    this.userSub = this.authService.user.subscribe( user => {
      console.log(user);
      this.isAuthenticated = !user ? true : false;
      // console.log(!user)
      // console.log(!!user)
    })
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

  toggleSideNav() {
   this.menuService.sidenav.toggle();
  }

  onLogout(){
    console.log("logout button pressed")
    this.authService.logoutUser().subscribe( res => {
      localStorage.removeItem('access_token')
      this.authService.removeUserOnLogout()
      this.router.navigate(['login'])
    })
    
  }
}

//for logout maybe
// .subscribe( res => {
//   console.log("logout button pressed")
//   this.router.navigate(['login'])
// })