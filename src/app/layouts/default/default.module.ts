//Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module'
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule} from '@angular/material/dialog';
import { AddclinicComponent } from 'src/app/modules/addclinic/addclinic.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';

//Components
import { LoginComponent } from 'src/app/modules/login/login.component';
import { DefaultComponent } from './default.component';
import { HomeComponent } from 'src/app/modules/home/home.component';
import { ClinicsComponent } from 'src/app/modules/clinics/clinics.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AuthInterceptorService } from 'src/app/Authentication/authServices/auth-interceptor.service';
import { ClinicsService } from 'src/app/modules/services/clinics.service';








@NgModule({
  declarations: [
    DefaultComponent,
    HomeComponent,
    ClinicsComponent,
    LoginComponent,
    AddclinicComponent,
    
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatGridListModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatMenuModule,
    FlexLayoutModule,
    MatSortModule,
    MatTableModule,
    MatDialogModule,
    MatExpansionModule,
    MatSnackBarModule
  ], 
 
  //Providers sørger for at klasser med metoder/funktioner bliver disponeret over hele appen.
  providers: [ClinicsService,
    {
               provide: HTTP_INTERCEPTORS,
               useClass: AuthInterceptorService,
               multi: true
              }]
})

export class DefaultModule { }
