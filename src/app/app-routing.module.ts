import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Components paths
import { LoginComponent } from './modules/login/login.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { DefaultComponent } from './layouts/default/default.component';
import { ClinicsComponent } from './modules/clinics/clinics.component';
import { HomeComponent } from './modules/home/home.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: '',
    component: DefaultComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'clinics', component: ClinicsComponent},
      
    ]
    
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
