import { HttpClient } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ClinicsService } from '../services/clinics.service';


@Injectable()
@Component({
  selector: 'app-addclinic',
  templateUrl: './addclinic.component.html',
  styleUrls: ['./addclinic.component.scss']
})
export class AddclinicComponent implements OnInit {

  constructor(public formRef: MatDialogRef<AddclinicComponent>, private http: HttpClient, private fb: FormBuilder, private clinicService: ClinicsService) { }

  //will also maybe only be needed when working with a model or entity
  // newClinic: clinic 

  clinicForm!: FormGroup
  
  ngOnInit(): void {
    this.clinicForm = this.fb.group(
    {
      clinicName: ['', Validators.required],
      acuteNumber: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      fax: ['',Validators.required]
      
    }
    )
  }

  onCancelClick(): void {
    this.formRef.close();
  }

  async onCreateClinic() {
    let clinicName = this.clinicForm.get('clinicName')?.value
    let acuteNumber = this.clinicForm.get('acuteNumber')?.value
    let phoneNumber = this.clinicForm.get('phoneNumber')?.value
    let fax = this.clinicForm.get('fax')?.value
    if(this.clinicForm.valid)
    this.clinicService.createClinic(clinicName, acuteNumber, phoneNumber, fax)
    .subscribe(responseData =>{
      this.formRef.close()
    });
  }



}


