import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../Authentication/authServices/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private fb: FormBuilder, private router: Router, private _snackBar: MatSnackBar) { 
  }

  //Vaiables
  loginForm!: FormGroup;
  isLoading = false;
  error: string = null
  durationInSeconds = 5;

  //One way of setting validation up. Check getErrorMessage() 
  email = new FormControl('', [Validators.required, Validators.email]);
  hide = true;
  
  ngOnInit(): void {
    this.loginForm = this.fb.group(
      {
      email: ['',[Validators.required, Validators.minLength(3), Validators.email]],
      password: ['',[ Validators.required]]
      }
    )
    
  }

  onSubmit(): void{
    let email = this.loginForm.get('email')?.value 
    let password = this.loginForm.get('password')?.value 
    this.isLoading = true
    if(this.loginForm.valid){
      this.authService.login(email, password).subscribe(res => {
        this.isLoading = false
        this.router.navigate([''])
        localStorage.setItem('access_token', res.user.access_token)
      }, errorMessage => {
        console.log(errorMessage);
        this.error = errorMessage;
        this.isLoading = false;
        this.openSnackBar()
        
        
      });

    this.loginForm.reset()
      
    }
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  openSnackBar() {
    this._snackBar.open(this.error, 'Luk',{duration: 10000}  )
  }





      
}
