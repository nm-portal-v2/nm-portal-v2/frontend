export interface Clinic { 
    clinicName: string;
    acuteNumber: Number;
    phoneNumber: Number;
    fax: Number;
    id?: string
}