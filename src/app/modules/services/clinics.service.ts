import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { exhaustMap, map, take } from 'rxjs/operators';
import { ApiService } from 'src/app/Authentication/authServices/api.service';
import { AuthService } from 'src/app/Authentication/authServices/auth.service';
import { Clinic } from '../models/clinics.model';


@Injectable({
  providedIn: 'root'
})
export class ClinicsService extends ApiService {

  loadedClinics: Clinic[] = []

  constructor(private http: HttpClient, private authService: AuthService) {
    super()
   }
  

  
  getClinicsFromDatabase(){
    return this.http.get<{ [key: string]: Clinic}>('http://localhost:3000/clinics', this.getHttpOptions()).pipe(map((responseData ) => {
      const clinicArray: Clinic[] = [];
      for(const key in responseData){
        if(responseData.hasOwnProperty(key)){
          clinicArray.push({ ...responseData[key], id: key})
        } 
      }
      return clinicArray;
    })
    )
  }

  createClinic(clinicName: string, acuteNumber: Number, phoneNumber: Number, fax: Number){
    return this.http.post('http://localhost:3000/clinics', {
      clinicName,
      acuteNumber,
      phoneNumber,
      fax
      
    })
  }



  
}
