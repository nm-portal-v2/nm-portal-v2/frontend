import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core'
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';;
import { MatTableDataSource } from '@angular/material/table';
import { AddclinicComponent } from '../addclinic/addclinic.component';
import { Clinic } from '../models/clinics.model';
import { ClinicsService } from '../services/clinics.service';


@Component({
  selector: 'app-clinics',
  templateUrl: './clinics.component.html',
  styleUrls: ['./clinics.component.scss']
})
export class ClinicsComponent implements OnInit {


  loadedClinics: Clinic[] = []
  displayedColumns: string[] = ['clinicName', 'acuteNumber', 'phoneNumber', 'fax'];
  dataSource: MatTableDataSource<any>
  panelOpenState = false;
  
  constructor(private http: HttpClient, public addForm: MatDialog, private clinicService: ClinicsService) { }

  @ViewChild(MatSort) sort: MatSort;
  

  ngOnInit(): void {
    this.getClinics()

  }

  openAddForm(){
    const formRef = this.addForm.open( AddclinicComponent, {
      // width: '250px'
    });
    formRef.afterClosed().subscribe(result => {
      console.log('form was closed')
    })
  }

  onGetClinics(){
    this.getClinics()
  }// måske bruges

 
  private getClinics(){
    this.clinicService.getClinicsFromDatabase()
    .subscribe(clinics => {
      console.log("After subscribe to data", clinics)
      this.dataSource  = new MatTableDataSource(clinics);
      this.dataSource.sort = this.sort;
    });
  }







}
