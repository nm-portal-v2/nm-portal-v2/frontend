import { Component, OnInit } from '@angular/core';
import { AuthService } from './Authentication/authServices/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'NM-PortalProject';

  constructor(private authService: AuthService){}

  ngOnInit(){
    this.authService.autoLogin();
  }
}
