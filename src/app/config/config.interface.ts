export interface Config {
    api:{
        target: string,
        secure: string,
        logLevel: string,
        changeOrigin: boolean,
        pathRewrite: {
            api: string}

  }
}// not being used