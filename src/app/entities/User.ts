export class User{
    role: string;
    firstname: string;
    lastname: string;
    birthdate: Date;
    email: string;
    password: string;

}