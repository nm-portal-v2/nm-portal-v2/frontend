import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import { BehaviorSubject, Subject, throwError } from 'rxjs';
import { catchError, reduce, tap } from 'rxjs/operators';
import { User } from '../models/user.model';
import { ApiService } from './api.service';


  // user = 
  interface AuthResponseData{
    user: {
      _id: string;
      email: string; 
      birthdate: string;
      lastName: string;
      firstName:string;
      expiresIn: number;
      access_token: string;
      currentHashedRefreshToken: string  }
    
    }

@Injectable({
  providedIn: 'root'
})
export class AuthService extends ApiService {

  user = new BehaviorSubject<User>(null)

  constructor(private http: HttpClient) {
    super()
  }
  
  private URL = 'http://localhost:3000'
  
  login(email: string, password: string){
    return this.http.post<AuthResponseData>(this.URL + '/auth/login',  {
      email,
      password, 
    }, this.getHttpOptions()).pipe(catchError(this.handleError), 
       tap(resData => {
       this.handleLoginAuthentication(resData.user.email, resData.user._id, resData.user.access_token, +resData.user.expiresIn)
       console.log(resData.user.access_token)
    })) 
  }

  logoutUser(){
    // Skal måske bare ramme metode istedet for endpoint
    console.log('logout test')
    return this.http.post(this.URL + '/auth/logout',{},this.getHttpOptions())
  }

  removeUserOnLogout(){
    this.user.next(null)
  }

  autoLogin(){
    const userData: {
      email: string;
      id: string;
      _token: string;
      _tokeExpirationDate: string;

    } = JSON.parse(localStorage.getItem('userData'))
    if(!userData){
      return; 
    }
    const loadedUser = new User(
      userData.email,
      userData.id,
      userData._token,
      new Date(userData._tokeExpirationDate)
    );

    if(loadedUser.token){
      this.user.next(loadedUser)
      //should mayebe implement a way to refresh the jwt token by calling our refresh request from th bacckend 
    }
  }

  // to handle all http response errors associated with authentication
  private handleError(errorRes: HttpErrorResponse){
    let errorMessage = 'Et problem opstod!';
    switch(errorRes.status){
      case 500:
        errorMessage = 'Et ukendt problem med serveren opstod!';
        break;
      case 401:
        errorMessage = 'Denne email eksistere ikke!';
        break;
      case 400:
        errorMessage = 'Fokert kode!';
        break;
    }
    return throwError(errorMessage);
  }

  private handleLoginAuthentication(email: string, userId: string, token: string, expiresIn: number){
    const expirationDate = new Date(
      new Date().getTime() + expiresIn * 1000
    );
    const user  = new User(email, userId, token, expirationDate);
    this.user.next(user)
    localStorage.setItem('userData', JSON.stringify(user))
    // console.log(user)
  }
  
}
