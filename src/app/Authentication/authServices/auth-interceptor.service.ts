import { HttpHandler, HttpHeaders, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { exhaustMap, take } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor( private authService: AuthService) { }

  
  intercept(req: HttpRequest<any>, next: HttpHandler){
    return this.authService.user.pipe(
      take(1),
      exhaustMap(user => {
        if(!user){
          
          return next.handle(req)
          
        }
        const headerReq = req.clone(
          {
            headers: new HttpHeaders(
              {
                'Authorization': 'Bearer ' + localStorage.getItem('access_token') //gonna try this with userData 
              }
            )
          }
        ) 
      return next.handle(headerReq);  
    }))
  }
}

